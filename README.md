The Simplest Rust Toolchain Installer
=====================================

[Read the article]
------------------

This tool is written in [literate programming] style,
using the [Tango] crate to convert between human-readable
and machine-readable versions.
The [human readable version](src/main.md) is checked into the repository,
the Rust source equivalent is generated at build time.

If you check out this repository and build it like a normal Rust project,
you can examine `src/main.rs`,
copy it into your own project,
and modify it to suit your needs.

[Read the article]: src/main.md
[literate programming]: https://en.wikipedia.org/wiki/Literate_programming
[Tango]: https://github.com/pnkfelix/tango
