The Simplest Rust Toolchain Installer
=====================================

For somebody that wants to write software in the Rust programming language,
getting started is pretty easy: the `rustup` toolchain installer will
automatically download all the required bits and pieces and configure your
system to make them available. However, the things that make `rustup` easy
to get started with can become drawbacks in a more industrial setting, so
there's a need for a more primitive and predictable toolchain-installer that
larger systems can be built on.

To help fill that need, I'm writing my own alternative toolchain manager
called [`rustbud`], but `rustbud` will have its own set of advantages
and drawbacks and there will always be somebody with a problem
that's not covered by existing solutions. For example, maybe you'd
rather a toolchain installer [based on Gradle's wrapper-script model][gradle],
or you want to build a custom toolchain you can [deploy offline][offline]. To
help people build tools that match their specific use-cases, I've published
all the libraries that `rustbud` is built on as separate Rust crates, and I'm
going to show you how to use them together to create the Simplest Rust Toolchain
Installer.

[gradle]: https://medium.com/@tibotz/embedding-rustup-into-cargo-and-the-wrapper-d5b447f381fc
[offline]: https://hatsunearu.github.io/2018/04/29/rust-offline/

Of course, like any superlative, "simplest" has some caveats:

  - The resulting installed toolchain may not be immediately convenient for
    humans to use, but it's straight-forward and predictable enough to build
    other systems on.
  - In the interests of brevity, there'll be no nuanced, detailed error
    handling. On the other hand, just about any problem can be fixed by
    deleting caches and trying again.

All the code in this article is dual-licensed under the MIT and Apache 2.0
licences, so feel free to use and build on it yourself.

[`rustbud`]: https://gitlab.com/Screwtapello/rustbud

Where we're going
-----------------

Before we get into how the tool works, let's look at what it does. Given
an install path, it will:

  - Download information about the current Rust stable release
  - Determine which artefact is most likely to work on this computer
    (for example, you don't want the Linux-for-ARM build on a
    Windows-for-x86 computer)
  - Download that artefact and extract it to a staging area
  - Install all the files from the staging area into the destination

In action, it looks something like this:

```text
$ cargo run /tmp/example-install-path/
     Running `target/debug/simplest-rust-toolchain-installer /tmp/example-install-path/`
Downloading channel metadata: https://static.rust-lang.org/dist/channel-rust-stable.toml
Found Rust version: 1.25.0 (84203cac6 2018-03-25)
Looking up artefact for x86_64-unknown-linux-gnu
Local install cache: "/home/st/.cache/simplest-rust-toolchain-installer/x86_64-unknown-linux-gnu/1.25.0 (84203cac6 2018-03-25)"
Downloading artefact: https://static.rust-lang.org/dist/2018-03-29/rust-1.25.0-x86_64-unknown-linux-gnu.tar.xz
Verifying hash: c206124130a329d4199b1711a857b8661f8159744a6ac3ba944e50d0e1cbba03
Installing to: "/tmp/example-install-path"
Installing component: cargo
Installing component: rls-preview
Installing component: rust-analysis-x86_64-unknown-linux-gnu
Installing component: rust-docs
Installing component: rust-std-x86_64-unknown-linux-gnu
Installing component: rustc
Installing component: rustfmt-preview
```

(this example was captured on a Linux machine, but the tool should work just as
well on Windows or macOS)

Once it's done, all the relevant files are now present:

```
$ ls /tmp/example-install-path/bin/
cargo
cargo-fmt
rls
rustc
rustdoc
rustfmt
rust-gdb
rust-lldb
```

To actually use this toolchain to build Rust software, there's a few more
steps you'll need to do manually:

  - Add the destination's `bin` directory to your `PATH` environment
    variable.
  - Point the `CARGO_HOME` environment variable at the destination
    directory, so this copy of the toolchain can have its own set of
    `cargo install`ed tools.
  - If you set `CARGO_HOME` and you want to publish to <https://crates.io/>
    with this toolchain, copy the `credentials` file from Cargo's regular
    home directory into the install location.

Note that all these steps involve changing an environment variable (which
is naturally isolated to a single process) or modifying the contents of the
destination directory itself, nothing outside it. Even if you're running a
build-server building a hundred projects in parallel, they can each have
their own Rust toolchain, completely isolated from each other.

Ingredients
-----------

We won't be writing our installer completely from scratch, we'll be using many
of the fine libraries and helpers the Rust community has built.

Rust's official toolchain binaries are hosted on an HTTP server, so we'll
need to be able to download URLs:

```rust
extern crate reqwest;
extern crate static_http_cache as shc;
extern crate url;
```

Every automated tool that downloads something from the Internet should validate
the hash, to ensure it's downloaded the right file:

```rust
extern crate crypto_hash;
extern crate hex;
```

We want to follow operating-system-specific conventions for where to stick
our local cache of downloads.

```rust
extern crate directories;
```

Most importantly, the libraries that will help us find, choose, and install
Rust toolchain releases.

```rust
extern crate guess_host_triple as ght;
extern crate rust_release_artefact as rra;
extern crate rust_release_channel as rrc;
```

Gathering Information
---------------------

Our `main()` function inspects the environment it's running in (including
environment variables, filesystem locations, and the operating system itself)
to help us choose the correct Rust toolchain and install it. This may seem
low-level and boring, but once it's out of the way, we'll be able to concentrate
on the install process itself.

```rust
fn main() {
```
We want to cache files we download and other information that's expensive to
produce, but we can always re-download or re-produce them if the user needs
to free up disk space. Some operating systems provide a special location
for exactly this kind of recreatable data, but the exact location varies from
operating system to operating system and computer to computer. Luckily, the
`directories` crate will figure it all out for us.

```rust
    let dirs = directories::ProjectDirs::from(
        "com.gitlab",
        "Screwtapello",
        "simplest-rust-toolchain-installer",
    );
    let local_cache = dirs.cache_dir();
```

The path we get from `directories` is probably an absolute path already, but
we're going to canonicalise it again just to make sure. Canonicalising a path
means that it will keep pointing to the same place even if the program calls
`std::env::set_current_dir()` later on; it means that errors and log messages
mentioning this path will be more informative; but most importantly on
Windows, Rust's canonicalisation function uses a syntax that happens to [raise
the path length limit][maxpath] from 260 to about 32,000 characters. Rust
toolchains can contain deeply-nested directory structures, and it's not possible
to reliably work with them without this kind of canonicalisation.

Rust can only canonicalise paths that already exist, so we need to create
the cache directory first. Because it's supposed to be a standard directory
provided by the operating system, it should be short enough that we can use
it directly in the `create_dir_all` call without canonicalisation.

[maxpath]: https://msdn.microsoft.com/en-us/library/windows/desktop/aa365247(v=vs.85).aspx#maxpath

```rust
    std::fs::create_dir_all(local_cache).expect("could not create cache dir");
    let local_cache = local_cache
        .canonicalize()
        .expect("could not canonicalize cache dir");
```

Rust's toolchain artefacts are available over HTTPS, but caching
HTTPS responses can be complex—there's a bunch of special rules for
detecting whether a given file has been updated on the server or not.
So instead of just downloading a file and keeping it forever, we use
the `static_http_cache` crate to manage requests for us... and tell it
to store its state inside the local cache directory that `directories`
picked out.

```rust
    let http_cache =
        shc::Cache::new(local_cache.join("http-cache"), reqwest::Client::new())
            .expect("Could not create HTTP cache");
```

For each release, Rust publishes a manifest describing all the packages
that are part of the release, the targets each package is built for, and
where the resulting build artefacts can be obtained. At least the
following manifests are available:

  - `channel-rust-stable.toml` describes the latest stable release
  - `channel-rust-beta.toml` describes the latest beta release
  - `channel-rust-nightly.toml` describes the latest nightly release
  - `channel-rust-1.25.0.toml` describes that specific stable release.
  - `2018-04-30/channel-rust-nightly.toml` describes the nightly build
    on a specific date.

In this program we're always going to install the latest stable release,
but if you want to use a particular historical release, or your code requires a
specific nightly release, it's easy to make that happen.

```rust
    let channel_url = url::Url::parse(
        "https://static.rust-lang.org/dist/channel-rust-stable.toml",
    ).expect("Could not parse channel URL");
```

The Rust toolchain is available for many different computers and
platforms, but we're only interested in a copy built to work on the
computer we're running on now. The Rust compiler (like many portable
compilers) uses [triples] to name the platforms it supports, so we
use the `guess_host_triple` crate to guess the triple that describes
this computer (that is, the computer that will "host" the toolchain).

Triples are not as rigorously defined as one might like, but `guess_host_triple`
is designed to follow Rust's conventions, so it should be fine.

[triples]: https://wiki.osdev.org/Target_Triplet

```rust
    let host_triple =
        ght::guess_host_triple().expect("Could not detect the host triple");
```

We accept a destination directory on the command-line. This code could
be more friendly if we were using a command-line parsing library, but
in the interests of keeping focussed on the actual installation process
we'll use the bare standard library and skip over this as quickly as
possible.

```rust
    let destination: std::path::PathBuf = std::env::args_os()
        .skip(1)
        .next()
        .expect("Provide destination directory on command-line")
        .into();
```

Much like the cache directory, we need to canonicalise the destination
directory too. We have no guarantees that the destination path is short enough
that we can create it before canonicalising it, but we don't have much choice.

```rust
    std::fs::create_dir_all(&destination)
        .expect("Could not create destination directory");
    let destination = destination
        .canonicalize()
        .expect("Could not canonicalize destination directory");
```

Now we've got everything we need to know: We have a directory on our
local disk where we can safely keep stuff; we have a way to retrieve
information from the network and *not* retrieve it if we already have a
local copy; we know where to find all the information we need about the
Rust toolchain; we know which platform version of the toolchain we need
to install; and we know where to install it. So let's go!

```rust
    install_rust(
        &local_cache,
        http_cache,
        channel_url,
        host_triple,
        &destination,
    ).expect("Could not install");
}
```

The Install Process
-------------------

The `main()` function has prepared all the information that we need to peform
the installation, and passed it to us in convenient data structures:

```rust
fn install_rust(
    local_cache: &std::path::Path,
    mut http_cache: shc::Cache<reqwest::Client>,
    channel_url: url::Url,
    host_triple: &str,
    destination: &std::path::Path,
) -> Result<(), Box<std::error::Error>> {
```
Since we have declared that this function returns a `Result`, we can use
the `?` operator to streamline error-handling. Because the error-type in
the `Result` is `Box<std::error::Error>`, we can skip creating our own
error-type and all the boiler-plate that involves. That makes error
values less useful to our caller, but we can glance up at the `main()`
function and see our caller just invokes the `.expect()` method anyway
so it's not missing out on much.

To find out about the Rust toolchain we'll be installing, we must
examine the channel manifest. We only have its URL, so we retrieve the
contents from our cache (which may download it as a side-effect). We get
back an open file-handle we can read from, which we then parse into a
`rust_release_channel::Channel`.

```rust
    eprintln!("Downloading channel metadata: {}", channel_url);
    let channel: rrc::Channel = {
        use std::io::Read;

        let mut handle = http_cache.get(channel_url)?;

        let mut buf = String::new();
        handle.read_to_string(&mut buf)?;

        buf.parse()?
    };
```

Now that we have the manifest data in a native data structure, we can
rummage around inside it for the packages we need. The release channel
describes many packages, like `rls` and `cargo`, but the special
name "rust" leads to a package that includes all the most-commonly-used
components of the Rust toolchain in a single download, so that's what
we'll use here.

```rust
    let package = channel
        .pkg
        .get("rust")
        .ok_or("Channel does not have a 'rust' package")?;

    eprintln!("Found Rust version: {}", package.version);
```

The `rust_release_channel::Package` struct contains all the information we need
to find the right Rust toolchain for this computer, download it and dig out the
installable files inside. We'll cover how all that works in the next section,
but for now we'll just say that the `ensure_extracted_artefact()` function takes
care of that, producing a `rust_release_artefact::ExtractedArtefact`.

```rust
    let extracted_artefact = ensure_extracted_artefact(
        local_cache,
        http_cache,
        package,
        host_triple,
    )?;
```

These artefacts include metadata that groups the installable files into named
"components", so you can pick and choose which components you want to install.
The `ExtractedArtefact` structure interprets that metadata and gives us
`Component` structs with a handy `.install_to()` method that (as you might
guess) installs that component's files to the correct locations in the
destination.

```rust
    eprintln!("Installing to: {:?}", destination);
    for (name, component) in extracted_artefact.components.iter() {
        eprintln!("Installing component: {}", name);
        component.install_to(destination)?;
    }
```

It's worth pointing out that `.install_to()` will not *copy* the files to the
destination if it can help it; instead, it prefers `std::fs::hard_link()` to
make the same file appear in both the source and destination directories. This
is much faster than copying, and means that the destination uses almost no
additional disk space—an important consideration if you're going to be making
a lot of isolated "copies" of a toolchain. Of course, hard-linking is not always
possible (for example, most operating systems cannot hard-link between separate
file-systems) and in that case `.install_to()` will fall back to a regular copy
operation.

And now, the installation process is complete!

```rust
    Ok(())
}
```

Well, except for the downloading-and-extracting operation we postponed earlier.

Downloading and Extracting
--------------------------

The `install_rust()` function above gives us a `Package` (representing the
version of the Rust toolchain it wants to install), a "host triple" (describing
the kind of computer we're running on), and some caches we can use to store
things if we need to.

In return, it wants either an `ExtractedArtefact` (a set of files on disk, ready
to be installed), or a good reason why not (the old familiar `Box<Error>`).

```rust
fn ensure_extracted_artefact(
    local_cache: &std::path::Path,
    mut http_cache: shc::Cache<reqwest::Client>,
    package: &rrc::Package,
    host_triple: &str,
) -> Result<rra::ExtractedArtefact, Box<std::error::Error>> {
```
How do we get from here to there?

Let's start with the `Package`. In the release channel manifest, a `Package`
represents a particular version of a particular piece of software - like "cargo
0.26.0" or "rustc 1.25.0". The `Package` struct contains various `Artefact`
structs, which represent copies of the package built for different computers. We
want one built for *this* computer.

```rust
    eprintln!("Looking up artefact for {}", host_triple);
    let artefact = package
        .target
        .get(host_triple)
        .ok_or("'rust' package not available for this host")?;
```

An `Artefact` is a set of files ready to be installed, so we'll need to
download an archive containing the artefact and extract it to obtain the files
we need. Or possibly... we won't. If this tool has previously run on this
computer, perhaps the archive we want has already been downloaded and extracted
somewhere, and we can re-use that location instead of doing all that work again.
If we decide on a convention for where to extract downloaded archives, we can
check that location and see if the artefact is already there. If it is, we can
use it immediately, otherwise we'll have to download it after all.

Artefacts are usually available in a choice of file-formats, but every
file-format contains the same files, so they'll be identical after extraction.
The only information we need to uniquely identify an artefact is the package
name (which we've hard-coded to be "rust" in this tool), the package version,
and the host triple.

```rust
    let expected_path = local_cache.join(host_triple).join(&package.version);
    std::fs::create_dir_all(&expected_path)?;
    eprintln!("Local install cache: {:?}", expected_path);
```

We put the host-triple before the package version because it's very likely that
this computer will only ever cache packages for one host-triple, but multiple
package versions. Doing it the other way around would require a host-triple
subdirectory inside every version subdirectory, which is a little wasteful.

Let's see if our expected path already contains an artefact:

```rust
    match rra::ExtractedArtefact::new(&expected_path) {
        Ok(extracted_artefact) => return Ok(extracted_artefact),
        Err(rra::Error::NoArtefacts(_)) => {}
        Err(e) => return Err(e.into()),
    }
```

The `ExtractedArtefact::new()` constructor examines the contents of the path
it's been given. If it looks like it contains a sensible extracted artefact,
we can assume we have exactly the `ExtractedArtefact` we wanted with almost zero
effort, and we just return it.

If `ExtractedArtefact::new()` complained that the directory contains no
artefacts at all, then we'll continue on and download and extract it ourselves.

Otherwise, `ExtractedArtefact::new()` returned some other error which means
there's already some kind of damaged or partially-extracted artefact in that
directory. Extracting another artefact on top of the existing mess probably
won't fix it, so we just return the error as-is.

At this point, we've run out of short-cuts, so it's time to download the
artefact... but from where? The `Artefact` struct contains an `ArchiveSource`
for each available format, and the `ArchiveSource` contains the URL we want.
Let's pick a format and download the artefact.

```rust
    let artefact_source = artefact
        .standalone
        .get(&rrc::ArchiveFormat::TarXz)
        .ok_or("'rust' package for this target not available as .tar.xz")?;

    eprintln!("Downloading artefact: {}", artefact_source.url);
    let mut artefact_handle = http_cache.get(artefact_source.url.clone())?;
```

In the above code snippet, we get the "standalone" version of the artefact.
As previously mentioned, an artefact can contain multiple components; the
`Artefact` struct also lists the required and optional components of this
artefact, and links to each component's `Artefact` record so you can download
them individually. Exactly which components are included in the "standalone"
artefact is not defined; usually it's the required components plus a few of the
most commonly-used optional ones. For the purposes of this tool we're just
taking the defaults, but if you want more control, it's available to you.

As well as the URL of the artefact archive, the `ArtefactSource` also contains
the SHA256 hash so we can verify that the file we downloaded is the one we
expected. The exact hashing process is pretty straight-forward, but if you want
the details you can see them in [Appendix A], below.

[Appendix A]: #appendix-a-file-hashing

```rust
    eprintln!("Verifying hash: {}", artefact_source.hash);
    let artefact_hash = sha256_hash(&mut artefact_handle)?;
    if artefact_source.hash != artefact_hash {
        Err(format!(
            "Downloaded artefact has hash {:?}, expected {:?}",
            artefact_hash, artefact_source.hash,
        ))?;
    }
```

We've downloaded the artefact, and it's exactly the one we wanted. Let's extract
it to the cache directory we prepared earlier, both for our caller to use
immediately, and for future invocations of this tool to re-use in the future.

```rust
    Ok(rra::ExtractedArtefact::from_tar_xz(
        std::io::BufReader::new(artefact_handle),
        expected_path,
    )?)
}
```

And now you know the basics of installing Rust toolchains!

Further reading
---------------

If you'd like to learn more about installing Rust toolchains, you may be
interested in reading some of these:

  - [rust_release_channel](https://docs.rs/rust_release_channel/)
    documentation
  - [rust_release_artefact](https://docs.rs/rust_release_artefact/)
    documentation
  - [guess_host_triple](https://docs.rs/guess_host_triple)
    documentation
  - [rustup distribution format](https://internals.rust-lang.org/t/a/4196)

Appendix A: File hashing
------------------------

We have an open, readable file-handle, and we want to produce the SHA256 hash of
the file's contents as a hexadecimal string, or a good reason why not.

```rust
fn sha256_hash(
    handle: &mut std::fs::File,
) -> Result<String, Box<std::error::Error>> {
```
Our file-handle is readable, the `Hasher` is writable, so we can just copy all
the data from one to the other.

```rust
    let mut hasher = crypto_hash::Hasher::new(crypto_hash::Algorithm::SHA256);
    std::io::copy(handle, &mut hasher)?;
```

The one wrinkle is that after the hashing is complete, the file's "current read
position" will be at the end of the file. Although we're done with the handle,
the extraction process still wants to read the file from the beginning, so we
should be kind and rewind:

```rust
    use std::io::Seek;
    handle.seek(std::io::SeekFrom::Start(0))?;
```

Now we can just get the hash, encode it as hexadecimal, and return it.

```rust
    Ok(hex::encode(hasher.finish()))
}
```
